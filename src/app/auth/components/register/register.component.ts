import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/users/user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userService: UserService;
  email: string;
  username: string;
  password: string;
  confirmPassword: string;

  constructor(private http: HttpClient, private router: Router) {
    this.userService = new UserService(http);
   }

  ngOnInit(): void {
    
  }

  async onRegisterClick() {
    const newUser = {email: this.email, username: this.username, password: this.password};  
    const user = await this.userService.createUser(newUser);
    if(user !== undefined) {
      this.router.navigate(["/login"]);
    }
  }

}
