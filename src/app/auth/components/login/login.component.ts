import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/users/user-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userService: UserService;
  email: string;
  password: string;

  constructor(private http: HttpClient, private router: Router) {
    this.userService = new UserService(http);
  }

  ngOnInit(): void {

  }

  onUserLogin() {
    const user = this.userService.searchUserByEmailAndPassword(this.email, this.password);
    if(user === undefined) {
      alert("Email or Password is wrong!");
    } else {
      sessionStorage.setItem("user", JSON.stringify(user));
      this.router.navigate(["/posts"]);
    }
  }
}
