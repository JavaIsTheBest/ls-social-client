import { PostsRoutingModule } from './../posts/posts-routing.module';
import { RouterModule } from '@angular/router';
import { UserComponent } from './components/user.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { HttpClientModule } from '@angular/common/http'


@NgModule({
  declarations: [UserComponent],
  imports: [
    CommonModule,
    RouterModule,
    UsersRoutingModule,
    PostsRoutingModule,
    HttpClientModule
  ],
  exports: [
    UserComponent
  ]
})
export class UsersModule { }
