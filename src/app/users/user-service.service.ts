import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from './user.model';

@Injectable()
export class UserService {

  private http: HttpClient;
  private users: Map<string, User> = new Map<string, User>();

  constructor(http: HttpClient) { 
    this.http = http;
    this.setupUsersServiceFromHTTP();
  }

  private setupUsersServiceFromHTTP() {
    this.http.get<User[]>(environment.apiUrl + "/users").subscribe(response => {
      const userArray = (response as User[]);
      userArray.map(user => this.users.set(user._id, user));
    });
  }

  createUser(newUser: User) {
    return this.http.post<User>(environment.apiUrl + "/users/create", {...newUser}).toPromise();
  }

  deleteUser(uuid: string) {
    this.http.delete(environment.apiUrl + "/users/delete/" + uuid).subscribe(data => {
      console.log(data);
    });
  }

  updateUser(uuid: string, updatedUser: User) {
    this.http.patch<User>(environment.apiUrl + "/users/update/" + uuid, {...updatedUser}).subscribe(response => {
      console.log(response);
    });
  }

  searchUserByEmailAndPassword(email: string, password: string) {
    for(const value of this.Users.values()){
      console.log(value.email + " - " + value.password);
      if(value.email === email && value.password === password) {
        return value;
      }
    }
    return undefined;
  }

  getUsernameByUUID(uuid: string) {
    if(this.users.get(uuid) === undefined)
      return "Loading";
    return this.users.get(uuid).username;
  }

  get Users() {
    return this.users;
  }
}
