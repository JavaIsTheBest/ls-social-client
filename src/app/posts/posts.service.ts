import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './post.model';

@Injectable()
export class PostsService {

  private http: HttpClient;
  private posts: Post[] = [];

  constructor(http: HttpClient) { 
    this.http = http;
  }

  getAllPosts() {
    return this.http.get<Post[]>(environment.apiUrl + "/posts").toPromise();
  }

  getNumberOfPosts(number: number) {
    return this.http.get<Post[]>(environment.apiUrl + "/posts/limit/" + number).toPromise();
  }

  createPost(newPost: Post, imageFile: File) {
    let formData = new FormData(); 
    formData.append("image", imageFile);
    for(let prop in newPost) {
      formData.append(prop, newPost[prop]);
    }

    return this.http.post<Post>(environment.apiUrl + "/posts/create", formData).toPromise();
  }

  updatePost(updatedPost: Post) {
    return this.http.patch<Post>(environment.apiUrl + "/posts/update/" + updatedPost._id, updatedPost).toPromise();
  }

  deletePost(uuid: string) {
    return this.http.delete<Post>(environment.apiUrl + "/posts/delete/" + uuid).toPromise();
  }

  async getPost(postID: string) {
    return await this.http.get<Post>(environment.apiUrl + "/posts/" + postID).toPromise();
  }

  get Posts() {
    return this.posts;
  }
}
