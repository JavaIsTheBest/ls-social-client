import { User } from 'src/app/users/user.model';
import { Post } from './../../post.model';
import { HttpClient } from '@angular/common/http';
import { PostsService } from './../../posts.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.component.html',
  styleUrls: ['./update-post.component.css']
})
export class UpdatePostComponent implements OnInit {

  currentUser: User = JSON.parse(sessionStorage.getItem("user"));
  imageUrl: string;
  postService: PostsService;
  currentPost: Post = null;

  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router) {
    this.postService = new PostsService(http);
  }

  async ngOnInit(): Promise<void> {
    if(this.currentUser === null) {
      this.router.navigate(["/login"]);
    } else {
      let postID = "";
      this.activatedRoute.queryParams.subscribe(params => {postID = params["postID"];});
      this.currentPost = await this.postService.getPost(postID);
      this.imageUrl = this.currentPost.imageUrl;
  
      if(this.currentPost.userUUID !== this.currentUser._id) {
        this.router.navigate(["/posts"]);
      }
    }
  }

  async onUpdatePostClick() {
    this.currentPost.updateDate = formatDate(new Date(), "dd/mm/yyyy", "en");
    const updation = await this.postService.updatePost(this.currentPost);
    if(updation) {
      this.router.navigate(["/posts"]);
    }
  }

  async onDeletePostClick(uuid: string) {
    if(confirm("Are you sure you want to delete this post?")) {
      await this.postService.deletePost(uuid);
      alert("Post has been deleted!");
      this.router.navigate(["/posts"]);
    }
  }

}
