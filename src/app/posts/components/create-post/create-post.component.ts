import { User } from 'src/app/users/user.model';
import { HttpClient } from '@angular/common/http';
import { PostsService } from './../../posts.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  currentUser: User = JSON.parse(sessionStorage.getItem("user"));
  fileReader: FileReader;
  postsService: PostsService;
  caption: string;
  description: string;
  image: File = null;
  imageUrl: String = "../../../assets/lsicon.jpg";

  creatingPost: boolean = false;

  constructor(private http: HttpClient, private router: Router) {
    this.postsService = new PostsService(http); 
    this.fileReader = new FileReader();
  }

  ngOnInit(): void {
    if(this.currentUser === null) {
      this.router.navigate(["/login"]);
    }
  }

  onFileSelected(event) {
    this.image = <File>event.target.files[0];
    this.fileReader.readAsDataURL(this.image);
    this.fileReader.onload = (e) => {
      this.imageUrl = e.target.result as String;
    }
  }

  async onPostCreationClick() {
    this.creatingPost = true;
    await this.postsService.createPost({
      caption: this.caption,
      likes: 0,
      description: this.description,
      userUUID: this.currentUser._id}, 
    this.image);
    
    this.router.navigate(["/posts"]); 
  }

}
