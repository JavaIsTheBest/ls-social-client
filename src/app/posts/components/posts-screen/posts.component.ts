import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Post } from '../../post.model';
import { PostsService } from '../../posts.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/users/user-service.service';
import { User } from 'src/app/users/user.model';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  postService: PostsService = null;
  userService: UserService = null;
  showingPosts: Post[] = [];
  currentUser: User = JSON.parse(sessionStorage.getItem("user"));

  maxium = 3;
  stopScrolling = false;
  notScrolly = true;

  constructor(private http: HttpClient, private router: Router, private spinner: NgxSpinnerService) {
    this.postService = new PostsService(http);
    this.userService = new UserService(http);
  }

  ngOnInit(): void {
    if(this.currentUser === null) {
      this.router.navigate(["/login"]);
    } else {
      this.loadInitPost();
    }
  }

  async loadInitPost() {
    this.showingPosts = await this.postService.getNumberOfPosts(this.maxium);
    this.maxium = this.showingPosts.length;
  }

  onScroll() {
    if(this.notScrolly && !this.stopScrolling) {
      this.spinner.show();
      this.notScrolly = false;
      this.loadNextPosts();
    }
  }

  async loadNextPosts() {
    const postArray = await this.postService.getNumberOfPosts(this.maxium + 1);
    console.log(postArray.length + " - " + this.maxium);
    if(postArray.length === this.maxium) {
      this.notScrolly = true;
      this.stopScrolling = true;
    } else {
      this.showingPosts.push(postArray[this.maxium]);
      this.notScrolly = true;
      this.maxium++;
      console.log(this.showingPosts);
    }
    this.spinner.hide();
  }
 
  onEditPostClick(clickedPost: Post) {
    console.log(this.userService.Users);
    this.router.navigate(["/posts/update/", clickedPost._id], {
      queryParams: { "postID": clickedPost._id }
    });
  }

  async onPostLikeClick(clickedPost: Post) {
    if(!clickedPost.whoLiked.includes(this.currentUser._id)) {
      clickedPost.likes += 1;
      clickedPost.whoLiked.push(this.currentUser._id);
      await this.postService.updatePost(clickedPost); 
    } else {
      if(clickedPost.likes - 1 <= 0) {
        clickedPost.likes = 0;
      } else {
        clickedPost.likes -= 1;
      }
      clickedPost.whoLiked.splice(clickedPost.whoLiked.indexOf(this.currentUser._id), 1);
      console.log(clickedPost.likes);
      await this.postService.updatePost(clickedPost); 
    }
  }

}
