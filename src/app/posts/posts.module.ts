import { NgxSpinnerModule } from 'ngx-spinner';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule } from '@angular/forms';
import { PostsComponent } from './components/posts-screen/posts.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsRoutingModule } from './posts-routing.module';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { UpdatePostComponent } from './components/update-post/update-post.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [PostsComponent, CreatePostComponent, UpdatePostComponent],
  imports: [
    CommonModule,
    PostsRoutingModule,
    FormsModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    BrowserAnimationsModule
  ],
  exports: [
    PostsComponent,
    CreatePostComponent,
    UpdatePostComponent
  ]
})
export class PostsModule { }
