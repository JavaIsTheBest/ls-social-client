export interface Post {
    _id?: string;
    userUUID: string;
    caption: string;
    description: string;
    imageUrl?: string;
    updateDate?: string;
    creationDate?: string;
    likes?: number;
    whoLiked?: string[];
}