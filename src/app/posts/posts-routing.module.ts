import { CreatePostComponent } from './components/create-post/create-post.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './components/posts-screen/posts.component';
import { UpdatePostComponent } from './components/update-post/update-post.component';

const routes: Routes = [
  { path: "posts", component: PostsComponent },
  { path: "posts/create", component: CreatePostComponent },
  { path: "posts/update/:uuid", component: UpdatePostComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
